﻿using TeamHeroCoderLibrary;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("Hello World!");

        GameClientConnectionManager connectionManager;
        connectionManager = new GameClientConnectionManager();
        connectionManager.SetExchangePath(MyAI.FolderExchangePath);
        connectionManager.onHeroHasInitiative = MyAI.ProcessAI;

        connectionManager.StartListeningToGameClientForHeroPlayRequests();
    }
}
