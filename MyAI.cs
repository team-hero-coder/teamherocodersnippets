using System;
using System.Collections.Generic;
using TeamHeroCoderLibrary;

public static class MyAI
{
    public static string FolderExchangePath = "Replace this text with the Team Hero Coder exchange directory";

    public static void ProcessAI()
    {
        Console.WriteLine("Processing AI!");

        #region SampleCode

        if (TeamHeroCoder.BattleState.heroWithInitiative.jobClass == HeroJobClass.Fighter)
        {
            Console.WriteLine("this is a fighter");
        }
        else if (TeamHeroCoder.BattleState.heroWithInitiative.jobClass == HeroJobClass.Cleric)
        {
            Console.WriteLine("this is a cleric");
        }
        else if (TeamHeroCoder.BattleState.heroWithInitiative.jobClass == HeroJobClass.Wizard)
        {
            Console.WriteLine("this is a wizard");
        }

        foreach (InventoryItem ii in TeamHeroCoder.BattleState.allyInventory)
        {
            if(ii.item == Item.Potion)
            {
                Console.WriteLine("Player has " + ii.count + " potion(s), total gold cost of player potions == " + ii.count * Info.ItemCost.Potion);
            }
        }

        //Searching for a poisoned hero 
        foreach (Hero hero in TeamHeroCoder.BattleState.allyHeroes)
        {
            foreach (StatusEffectAndDuration se in hero.statusEffectsAndDurations)
            {
                if (se.statusEffect == StatusEffect.Poison)
                {
                    Console.WriteLine(hero.jobClass + " is poisoned");
                }
            }
        }

        //Find the foe that is not KO and has the lowest health
        Hero target = null;

        foreach (Hero hero in TeamHeroCoder.BattleState.foeHeroes)
        {
            if (hero.health > 0)
            {
                if (target == null)
                    target = hero;
                else if (hero.health < target.health)
                    target = hero;
            }
        }

        Console.WriteLine(target.jobClass + " has been select as lowest health target.");

        //This is the line of code that tells Team Hero Coder that we want to perform the attack action targeting the foe with the lowest HP
        TeamHeroCoder.PerformHeroAbility(Ability.Attack, target);

        #endregion

        #region Extended Sample Code Readings

        int dmgAmount = Utility.CalculateDamageAmount(TeamHeroCoder.BattleState.heroWithInitiative, Info.AbilityModifersAndParams.AttackDamageMod, target, true);
        Console.WriteLine("Unless intercepted by cover, the amount of damage that will be done == " + dmgAmount);


        if (Utility.AreAbilityAndTargetLegal(Ability.MagicMissile, target, false))
            Console.WriteLine("The hero with initiative is capable of casting Magic Missile.");
        else
            Console.WriteLine("The hero with initiative is not capable of casting Magic Missile.");


        if (Utility.AreAbilityAndTargetLegal(Ability.Meteor, null, false))
            Console.WriteLine("The hero with initiative is capable of casting Meteor.");
        else
            Console.WriteLine("The hero with initiative is not capable of casting Meteor.");


        List<Hero> playerHeroesThatAreNotKOAndHaveHealthLessThanPercentAmount = new List<Hero>();
        foreach (Hero hero in TeamHeroCoder.BattleState.allyHeroes)
        {
            if (hero.health <= 0)
                continue;

            float p = (float)hero.health / (float)hero.maxHealth;
            if (p < 0.4f)
                playerHeroesThatAreNotKOAndHaveHealthLessThanPercentAmount.Add(hero);
        }


        List<Hero> playerHeroesThatAreNotKOAndHaveManaLessThanPercentAmount = new List<Hero>();
        foreach (Hero hero in TeamHeroCoder.BattleState.allyHeroes)
        {
            if (hero.health <= 0)
                continue;

            float p = (float)hero.mana / (float)hero.maxMana;
            if (p < 0.4f)
                playerHeroesThatAreNotKOAndHaveManaLessThanPercentAmount.Add(hero);
        }


        List<Hero> foeHeroesWithRogueHeroClass = new List<Hero>();
        foreach (Hero hero in TeamHeroCoder.BattleState.foeHeroes)
        {
            if (hero.jobClass == HeroJobClass.Rogue)
                foeHeroesWithRogueHeroClass.Add(hero);
        }


        List<Hero> foeHeroesWithWizardEvokerPerk = new List<Hero>();
        foreach (Hero h in TeamHeroCoder.BattleState.foeHeroes)
        {
            foreach (Perk p in h.perks)
            {
                if (p == Perk.WizardEvoker)
                    foeHeroesWithWizardEvokerPerk.Add(h);
            }
        }


        List<Hero> foeHeroesWithCoverPassiveAbility = new List<Hero>();
        foreach (Hero h in TeamHeroCoder.BattleState.foeHeroes)
        {
            foreach (PassiveAbility p in h.passiveAbilities)
            {
                if (p == PassiveAbility.Cover)
                    foeHeroesWithCoverPassiveAbility.Add(h);
            }
        }


        #endregion

    }

}